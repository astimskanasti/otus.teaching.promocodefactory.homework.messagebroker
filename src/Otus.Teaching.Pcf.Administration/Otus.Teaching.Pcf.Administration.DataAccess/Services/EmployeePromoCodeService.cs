using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Services
{
    public class EmployeePromoCodeService : IEmployeePromoCodeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeePromoCodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        public async Task<bool> UpdateAppliedPromoCodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null) return false;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}