using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;
using Otus.Teaching.Pcf.Administration.Integration.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumer
{
    public class RabbitMsg
    {
        int Id { get; set; }
        string Name { get; set; }
    }
    
    public class EventConsumer : IConsumer<RabbitMsg>
    {
        /*
        private readonly IEmployeePromoCodeService _employeeService;

        public EventConsumer(IEmployeePromoCodeService employeeService)
        {
            _employeeService = employeeService;
        }*/

        public async Task Consume(ConsumeContext<RabbitMsg> context)
        {
            
            Console.Write("CONSUMERCONSUME");
            int a = 5;
            if (context.Message.GetHashCode() > 0)
            {
                a++;
            }
            // if (context.Message.PartnerManagerId.HasValue)
            // {
            //     Guid partnerManagerId = context.Message.PartnerManagerId.Value;
            //await _employeeService.UpdateAppliedPromoCodesAsync(partnerManagerId);
            //}
        }
    }
}