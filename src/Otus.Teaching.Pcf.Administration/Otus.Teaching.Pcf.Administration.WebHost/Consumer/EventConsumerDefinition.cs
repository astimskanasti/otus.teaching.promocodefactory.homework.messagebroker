using MassTransit;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumer
{
    public class EventConsumerDefinition: ConsumerDefinition<EventConsumer>
    {
        public EventConsumerDefinition()
        {
            EndpointName = "Test";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<EventConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}