using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions
{
    public interface IEmployeePromoCodeService
    {
        public Task<bool> UpdateAppliedPromoCodesAsync(Guid id);
    }
}