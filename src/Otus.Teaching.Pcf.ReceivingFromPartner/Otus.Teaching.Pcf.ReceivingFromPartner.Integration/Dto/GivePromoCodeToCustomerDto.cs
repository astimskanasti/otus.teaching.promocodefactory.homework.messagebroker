﻿using System;
using MassTransit;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class GivePromoCodeToCustomerDto
    {
        public int id { get; set; }
    }
}